package rs.etf.iep.soap;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Spinner;
import android.widget.TextView;

public class ServiceTaskModels extends AsyncTask<String, Void, String> {

	ProgressDialog progress;
	String response = "";
	//public final String SOAP_ACTION = "http://autooglasi.com/iep/GetMakers";
	private static String METHOD_NAME = "GetModels";
	private static final String NAMESPACE = "http://autooglasi.com/iep";
	public  String SOAP_ACTION = "http://autooglasi.com/iep/GetModels";
	public final String URL = "http://109.106.254.31:8080/AutoOglasiWebService.asmx";
	
	
	
	private String res;



	// private Spinner spinner;
	// private MyHash myHash;

	public void onPreExecute() {
		
		super.onPreExecute();
	}

	public ServiceTaskModels( ) {
	}

	
	@Override
	protected String doInBackground(String... params) {
		try {


			
			int makerID = Integer.parseInt(params[1]);
			String ID = params[1];
			SoapObject request = new SoapObject(NAMESPACE,METHOD_NAME);
			
			PropertyInfo pi = new PropertyInfo();

			Log.i("NENAD MAKERID ", makerID+"");
			
			pi.setName("i");
			pi.setValue(makerID);
			pi.setType(Integer.class);
			request.addProperty(pi);
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE httpTransport = new HttpTransportSE(URL);
			Object response = null;
			try {
				httpTransport.call(SOAP_ACTION, envelope);
				response = envelope.getResponse();
				res = response.toString();
			} catch (Exception exception) {
				//response = exception.toString()
				Log.e("NENADGRESKA ", exception.toString());
			}
			
		} catch (Exception e) {
			Log.e("ERROR: ", e.getMessage());
		}

		return res;
	}

	@Override
	public void onPostExecute(String res) {
		
		
		
		if (res != null) {
		//	tv.setText("SVE JE OK");
		}
		super.onPostExecute(res);
	}

}
