package rs.etf.iep.soap;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import rs.etf.iep.autooglasi.MainActivity;
import rs.etf.iep.autooglasi.SettingsActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Spinner;
import android.widget.TextView;

public class ServiceTask extends AsyncTask<String, Void, String> {

	String response = "";
	//public final String SOAP_ACTION = "http://autooglasi.com/iep/GetMakers";
	private static String METHOD_NAME = "GetMakers";
	private static final String NAMESPACE = "http://autooglasi.com/iep";
	public  String SOAP_ACTION = "http://autooglasi.com/iep/GetMakers";
	public final String URL = "http://109.106.254.31:8080/AutoOglasiWebService.asmx";
	
	
	private String[] resultValue;
	private String res;


	private Context context;
	private TextView tv;

	// private Spinner spinner;
	// private MyHash myHash;

	public void onPreExecute() {
		
		super.onPreExecute();
	}

	public ServiceTask() {

	}

	
	@Override
	protected String doInBackground(String... params) {
		try {

			METHOD_NAME = params[0];
			SOAP_ACTION = NAMESPACE + "/" + METHOD_NAME;
								
			SoapObject request = new SoapObject(NAMESPACE,METHOD_NAME);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE httpTransport = new HttpTransportSE(URL);
			Object response = null;
			try {
				httpTransport.call(SOAP_ACTION, envelope);
				response = envelope.getResponse();
				res = response.toString();
			} catch (Exception exception) {
				//response = exception.toString()
				Log.e("NENADGRESKA ", exception.toString());
			}
			
		} catch (Exception e) {
			Log.e("ERROR: ", e.getMessage());
		}

		return res;
	}

	@Override
	public void onPostExecute(String res) {
	
		super.onPostExecute(res);
	}

}
