package rs.etf.iep.soap;

import java.util.HashMap;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import rs.etf.iep.autooglasi.MainActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Spinner;
import android.widget.TextView;

public class ServiceTaskSearchAds extends AsyncTask<HashMap<String, String>, Void, String> {

	ProgressDialog progress;
	String response = "";
	//public final String SOAP_ACTION = "http://autooglasi.com/iep/GetMakers";
	private static String METHOD_NAME = "Search";
	private static final String NAMESPACE = "http://autooglasi.com/iep";
	public  String SOAP_ACTION = "http://autooglasi.com/iep/Search";
	public final String URL = "http://109.106.254.31:8080/AutoOglasiWebService.asmx";
	
	
	
	private String res;



	// private Spinner spinner;
	// private MyHash myHash;

	public void onPreExecute() {
		
		super.onPreExecute();
	}

	public ServiceTaskSearchAds( ) {
	}

	
	@Override
	protected String doInBackground(HashMap<String, String>... params) {
		try {
			
			HashMap<String, String> parametri = params[0];
			
			SoapObject request = new SoapObject(NAMESPACE,METHOD_NAME);
			
			PropertyInfo pi1 = new PropertyInfo();
			pi1.setName(MainActivity.KEY_ID_MAKERS);
			pi1.setValue(parametri.get(MainActivity.KEY_ID_MAKERS));
			pi1.setType(String.class);
			request.addProperty(pi1);
			PropertyInfo pi2 = new PropertyInfo();
			pi2.setName(MainActivity.KEY_ID_MODELS);
			pi2.setValue(parametri.get(MainActivity.KEY_ID_MODELS));
			pi2.setType(String.class);
			request.addProperty(pi2);
			PropertyInfo pi3 = new PropertyInfo();
			pi3.setName(MainActivity.KEY_ID_BODY);
			pi3.setValue(parametri.get(MainActivity.KEY_ID_BODY));
			pi3.setType(String.class);
			request.addProperty(pi3);
			PropertyInfo pi4 = new PropertyInfo();
			pi4.setName(MainActivity.KEY_ID_FUEL);
			pi4.setValue(parametri.get(MainActivity.KEY_ID_FUEL));
			pi4.setType(String.class);
			request.addProperty(pi4);
			PropertyInfo pi5 = new PropertyInfo();
			pi5.setName(MainActivity.KEY_ID_YEAR_FROM);
			pi5.setValue(parametri.get(MainActivity.KEY_ID_YEAR_FROM));
			pi5.setType(String.class);
			request.addProperty(pi5);
			PropertyInfo pi6 = new PropertyInfo();
			pi6.setName(MainActivity.KEY_ID_YEAR_TO);
			pi6.setValue(parametri.get(MainActivity.KEY_ID_YEAR_TO));
			pi6.setType(String.class);
			request.addProperty(pi6);
			PropertyInfo pi7 = new PropertyInfo();
			pi7.setName(MainActivity.KEY_ID_PRICE_FROM);
			pi7.setValue(parametri.get(MainActivity.KEY_ID_PRICE_FROM));
			pi7.setType(String.class);
			request.addProperty(pi7);
			PropertyInfo pi8 = new PropertyInfo();
			pi8.setName(MainActivity.KEY_ID_PRICE_TO);
			pi8.setValue(parametri.get(MainActivity.KEY_ID_PRICE_TO));
			pi8.setType(String.class);
			request.addProperty(pi8);
			
			
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE httpTransport = new HttpTransportSE(URL);
			Object response = null;
			try {
				httpTransport.call(SOAP_ACTION, envelope);
				response = envelope.getResponse();
				res = response.toString();
			} catch (Exception exception) {
				//response = exception.toString()
				Log.e("NENADGRESKA ", exception.toString());
			}
			
		} catch (Exception e) {
			Log.e("ERROR: ", e.getMessage());
		}

		return res;
	}

	@Override
	public void onPostExecute(String res) {
		
		
		
		if (res != null) {
		//	tv.setText("SVE JE OK");
		}
		super.onPostExecute(res);
	}

}
