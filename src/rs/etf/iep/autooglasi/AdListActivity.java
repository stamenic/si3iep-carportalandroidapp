package rs.etf.iep.autooglasi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import rs.etf.iep.autooglasi.helper.XMLParser;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class AdListActivity extends ListActivity {

	List<Ad> oglasiZaPrikaz = new ArrayList<Ad>();
	ArrayAdapter<Ad> adapterListe = null;
	
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent intent = getIntent();    
		HashMap<String, String> parametri = (HashMap<String, String>) intent.getSerializableExtra(MainActivity.KEY_PARAM);
		
		adapterListe = new AdListAdapter(this, populateDataModel(parametri));
		
		setListAdapter(adapterListe);
		adapterListe.notifyDataSetChanged();
	}
	
	protected List<Ad> populateDataModel(HashMap<String, String> parametri) {
		// Fetch data from a remote server and populate
		// the model. 
		oglasiZaPrikaz.clear();
		

		XMLParser parser = new XMLParser();
		String xml = parser.getXmlFromSOAPSearch(parametri); // getting
																		// XML
		Document doc = parser.getDomElement(xml); // getting DOM element

		NodeList nl = doc.getElementsByTagName(MainActivity.KEY_ITEM_ADS);
		// looping through all item nodes <item>
		for (int i = 0; i < nl.getLength(); i++) {
			Element e = (Element) nl.item(i);
			Ad a = new Ad();
			a.setIDAd(Integer.parseInt(parser.getValue(e, MainActivity.KEY_ID_AD)));
			a.setMaker(parser.getValue(e, MainActivity.KEY_NAME_MAKERS));
			a.setModel(parser.getValue(e, MainActivity.KEY_NAME_MODELS));
			a.setBody(parser.getValue(e, MainActivity.KEY_NAME_BODY));
			a.setFuel(parser.getValue(e, MainActivity.KEY_NAME_FUEL));
			a.setPrice(Float.parseFloat(parser.getValue(e, MainActivity.KEY_NAME_PRICE)));
			a.setYear(Integer.parseInt(parser.getValue(e, MainActivity.KEY_NAME_YEAR)));
			a.setIDPicture((parser.getValue(e, MainActivity.KEY_NAME_PICTURE)));
			String string = parser.getValue(e, MainActivity.KEY_NAME_ADDATE);
			String[] parts = string.split("T");
			String datum = parts[0]; // 2014-02-04
			String vreme = parts[1]; // 19:49:15+01:00			
			Date date = new Date();
			try {
				date = new SimpleDateFormat("yyyy-MM-dd").parse(datum);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			a.setAdDate(date);
			oglasiZaPrikaz.add(a);
		}

		
		return oglasiZaPrikaz;
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		Ad selectedAd = oglasiZaPrikaz.get(position);
		Intent intent = new Intent(AdListActivity.this, AdActivity.class);
		intent.putExtra(MainActivity.KEY_ID_AD,selectedAd);
		startActivity(intent);
		
	}
}
