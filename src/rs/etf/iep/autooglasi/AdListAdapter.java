package rs.etf.iep.autooglasi;


import java.util.List;

import rs.etf.iep.autooglasi.helper.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdListAdapter extends ArrayAdapter<Ad> {
	

	Activity context;
	View fragment;
	private ImageLoader img;
	
	static class ViewHolder {
		TextView ModelMaker, Year, Body, Fuel,Price;
		ImageView Picture;
	}
	
	public AdListAdapter(Context ctx, List<Ad> objects) {
		super(ctx, R.layout.ad_row, objects);
		this.context = (Activity) ctx;
		this.img = new ImageLoader(context);
		
		
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = null;
		if (convertView == null) {
			LayoutInflater inflater = context.getLayoutInflater();	
			view = inflater.inflate(R.layout.ad_row, null);
			
			// Optimization: view object caching
			ViewHolder vh = new ViewHolder();
			vh.ModelMaker = (TextView) view.findViewById(R.id.AdTitle);
			vh.Year = (TextView) view.findViewById(R.id.AdYear);
			vh.Body = (TextView) view.findViewById(R.id.AdBody);
			vh.Fuel = (TextView) view.findViewById(R.id.AdFuel);
			vh.Price = (TextView) view.findViewById(R.id.AdPrice);
			vh.Picture = (ImageView) view.findViewById(R.id.list_image);			
			
			view.setTag(vh);
		}
		else {
			view = convertView;
		}
		
		ViewHolder vh = (ViewHolder) view.getTag();
		
		Ad ad = getItem(position);
		vh.ModelMaker.setText(ad.getMaker()+ " " + ad.getModel());
		vh.Year.setText(ad.getYear()+ "");
		vh.Body.setText(ad.getBody());
		vh.Fuel.setText(ad.getFuel());
		vh.Price.setText(ad.getPriceString()+ "�");
		
		img.displayImage("http://109.106.254.31:8080/images/ads/"+ad.getIDPicture()+".jpg", vh.Picture);
		
		return view;
	}
	
	
}