package rs.etf.iep.autooglasi.helper;

import java.io.File;

import android.content.Context;

/**
 * @author Tahir Slezovic
 * @version 1.0
 */
public class FileCache {

	public File cacheDir;

	public FileCache(Context context) {
		try {
			if (android.os.Environment.getExternalStorageState().equals(
					android.os.Environment.MEDIA_MOUNTED))
				cacheDir = new File(
						android.os.Environment.getExternalStorageDirectory(),
						"AutoOglasi"+"/"+"img");
			else
				cacheDir = context.getCacheDir();
			//Log.i("PUTANJA", cacheDir.getPath());
			if (!cacheDir.exists())
				cacheDir.mkdirs();
		} catch (Exception e) {
			cacheDir = null;

		}
	}

	public File getFile(String url) {
		if (cacheDir == null)
			return null;
		String filename = String.valueOf(url.hashCode());
		File f = new File(cacheDir, filename);
		return f;

	}

	public void clear() {
		File[] files = cacheDir.listFiles();
		if (files == null)
			return;
		for (File f : files)
			f.delete();
	}

}