package rs.etf.iep.autooglasi;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import rs.etf.iep.autooglasi.helper.XMLParser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	/*
	 * private static final String METHOD_NAME = "/GetMakers"; private static
	 * final String NAMESPACE = "http://autooglasi.com/iep"; String SOAP_ACTION
	 * = "http://autooglasi.com/iep/GetMakers"; String URL =
	 * "http://212.178.249.219:8080/AutoOglasiWebService.asmx";
	 */

	// XML node keys
	public static final String KEY_ITEM_MAKERS = "Makers";
	public static final String KEY_ID_MAKERS = "IDMaker";
	public static final String KEY_NAME_MAKERS = "MakerName";
	public static final String KEY_ITEM_MODELS = "models";
	public static final String KEY_ID_MODELS = "IDModel";
	public static final String KEY_NAME_MODELS = "ModelName";
	public static final String KEY_ITEM_BODY = "bodytypes";
	public static final String KEY_ID_BODY = "IDBodyType";
	public static final String KEY_NAME_BODY = "BodyTypeName";
	public static final String KEY_ITEM_FUEL = "fueltypes";
	public static final String KEY_ID_FUEL = "IDFuelType";
	public static final String KEY_NAME_FUEL = "FuelTypeName";
	public static final String KEY_ID_YEAR_FROM = "YearFrom";
	public static final String KEY_ID_YEAR_TO = "YearTo";
	public static final String KEY_ID_PRICE_FROM = "PriceFrom";
	public static final String KEY_ID_PRICE_TO = "PriceTo";
	public static final String KEY_PARAM = "parametri";
	public static final String KEY_ITEM_ADS = "Ads";
	public static final String KEY_ITEM_AD = "Ad";
	public static final String KEY_ID_AD = "IDAd";
	public static final String KEY_NAME_PRICE = "Price";
	public static final String KEY_NAME_YEAR = "Year";
	public static final String KEY_NAME_ADDATE = "AdDate";
	public static final String KEY_NAME_PICTURE = "IDPicture";
	
	ArrayList<String> makersIDs = new ArrayList<String>();
	ArrayList<String> modelsIDs = new ArrayList<String>();
	ArrayList<String> bodysIDs = new ArrayList<String>();
	ArrayList<String> fuelsIDs = new ArrayList<String>();
	ArrayList<String> makersNames = new ArrayList<String>();
	ArrayList<String> modelsNames = new ArrayList<String>();
	ArrayList<String> bodysNames = new ArrayList<String>();
	ArrayList<String> fuelsNames = new ArrayList<String>();

	ArrayList<String> YearsFrom = new ArrayList<String>();
	ArrayList<String> YearsTo = new ArrayList<String>();

	Spinner spinnerMakers;
	Spinner spinnerModels;
	Spinner spinnerBody;
	Spinner spinnerFuel;
	Spinner spinnerYearFrom;
	Spinner spinnerYearTo;
	EditText etPriceFrom;
	EditText etPriceTo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);

		spinnerMakers = (Spinner) findViewById(R.id.spinnerMakers);
		spinnerModels = (Spinner) findViewById(R.id.spinnerModels);
		spinnerBody = (Spinner) findViewById(R.id.spinnerBody);
		spinnerFuel = (Spinner) findViewById(R.id.spinnerFuel);
		spinnerYearFrom = (Spinner) findViewById(R.id.spinnerYearFrom);
		spinnerYearTo = (Spinner) findViewById(R.id.spinnerYearTo);
		etPriceFrom = (EditText) findViewById(R.id.editTextPriceFrom);
		etPriceTo= (EditText) findViewById(R.id.editTextPriceTo);

		populateSpinners();

		etPriceFrom.setHint("od");
		etPriceTo.setHint("do");
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	 @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        switch (item.getItemId()) {
	        case R.id.action_settings:
	        	Intent intent = new Intent(getBaseContext(), SettingsActivity.class);
	            startActivity(intent);
	            return true;  }
	        return super.onOptionsItemSelected(item);
	    }
	
	private void populateSpinners() {
		ArrayList<String> xmls = new ArrayList<String>();
		XMLParser parser = new XMLParser();
		xmls = parser.getXmls();
		// String xml = parser.getXmlFromSOAP("GetMakers",this); // getting XML
		// getting DOM elements
		Document docMakers = parser.getDomElement(xmls.get(0)); 
		Document docBodys = parser.getDomElement(xmls.get(1)); 
		Document docFuels = parser.getDomElement(xmls.get(2)); 
		
		//get data from xmls and populate Arrays and Spinners
		populateArrayMakers(parser, docMakers);
		populateArrayBodys(parser, docBodys);
		populaterArrayFuels(parser, docFuels);
		populaterYears();

	}

	private void populateArrayMakers(XMLParser parser, Document doc) {
		NodeList nl = doc.getElementsByTagName(KEY_ITEM_MAKERS);
		makersIDs.add(0, "0");
		makersNames.add(0, "Svi proizvodjaci");
		// looping through all item nodes <item>
		for (int i = 0; i < nl.getLength(); i++) {
			Element e = (Element) nl.item(i);
			makersIDs.add(i + 1, parser.getValue(e, KEY_ID_MAKERS));
			makersNames.add(i + 1, parser.getValue(e, KEY_NAME_MAKERS));
		}
		setSpinnerMakers(spinnerMakers, makersNames);
	}

	private void populateArrayBodys(XMLParser parser, Document doc) {
		NodeList nl = doc.getElementsByTagName(KEY_ITEM_BODY);
		bodysIDs.add(0, "0");
		bodysNames.add(0, "Sve karoserije");
		// looping through all item nodes <item>
		for (int i = 0; i < nl.getLength(); i++) {
			Element e = (Element) nl.item(i);
			bodysIDs.add(i + 1, parser.getValue(e, KEY_ID_BODY));
			bodysNames.add(i + 1, parser.getValue(e, KEY_NAME_BODY));
		}
		setSpinner(spinnerBody, bodysNames);
	}

	private void populaterArrayFuels(XMLParser parser, Document doc) {
		NodeList nl = doc.getElementsByTagName(KEY_ITEM_FUEL);
		fuelsIDs.add(0, "0");
		fuelsNames.add(0, "Sva goriva");
		// looping through all item nodes <item>
		for (int i = 0; i < nl.getLength(); i++) {
			Element e = (Element) nl.item(i);
			fuelsIDs.add(i + 1, parser.getValue(e, KEY_ID_FUEL));
			fuelsNames.add(i + 1, parser.getValue(e, KEY_NAME_FUEL));
		}
		setSpinner(spinnerFuel, fuelsNames);
	}

	private void populaterYears() {

		YearsFrom.add(0, "od");
		YearsTo.add(0, "do");
		for (int i = 1960; i <= Calendar.getInstance().get(Calendar.YEAR); i++) {
			YearsFrom.add(Integer.toString(i));
		}

		for (int i = Calendar.getInstance().get(Calendar.YEAR); i > 1960; i--) {
			YearsTo.add(Integer.toString(i));
		}

		setSpinner(spinnerYearFrom, YearsFrom);
		setSpinner(spinnerYearTo, YearsTo);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	void setSpinnerMakers(Spinner spinner, ArrayList<String> lista) {
		ArrayAdapter dataAdapter = new ArrayAdapter(getBaseContext(),
				android.R.layout.simple_spinner_item, lista);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);

		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				Object item = arg0.getItemAtPosition(arg2);
				if (item != null) {
					Log.i("ARGUMENT", arg2 + "");
					getModels(arg2);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	void setSpinner(Spinner spinner, ArrayList<String> lista) {
		ArrayAdapter dataAdapter = new ArrayAdapter(getBaseContext(),
				android.R.layout.simple_spinner_item, lista);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
	}

	private void getModels(int idMaker) {
		if (idMaker == 0) {
			spinnerModels.setEnabled(false);
		}

		else {
			spinnerModels.setEnabled(true);

			XMLParser parser = new XMLParser();
			String xml = parser.getXmlFromSOAPModels("GetModels", idMaker); // getting
																			// XML
			Document doc = parser.getDomElement(xml); // getting DOM element

			NodeList nl = doc.getElementsByTagName(KEY_ITEM_MODELS);
			modelsIDs.add(0, "0");
			modelsNames.add(0, "Svi modeli");
			// looping through all item nodes <item>
			for (int i = 0; i < nl.getLength(); i++) {
				Element e = (Element) nl.item(i);
				modelsIDs.add(i + 1, parser.getValue(e, KEY_ID_MODELS));
				modelsNames.add(i + 1, parser.getValue(e, KEY_NAME_MODELS));
			}

			setSpinner(spinnerModels, modelsNames);

		}
	}

	
	public void pretraga(View v) {
		HashMap<String, String> parametri = new HashMap<String, String>();
		parametri.put(KEY_ID_MAKERS, makersIDs.get(spinnerMakers.getSelectedItemPosition()));
		if(spinnerMakers.getSelectedItemPosition()!=0) parametri.put(KEY_ID_MODELS, modelsIDs.get(spinnerModels.getSelectedItemPosition()));
		else parametri.put(KEY_ID_MODELS, "0");
		parametri.put(KEY_ID_BODY, bodysIDs.get(spinnerBody.getSelectedItemPosition()));
		parametri.put(KEY_ID_FUEL, fuelsIDs.get(spinnerFuel.getSelectedItemPosition()));
		if(spinnerYearFrom.getSelectedItemPosition()==0)parametri.put(KEY_ID_YEAR_FROM, "0");
		else parametri.put(KEY_ID_YEAR_FROM, YearsFrom.get(spinnerYearFrom.getSelectedItemPosition()));
		if(spinnerYearTo.getSelectedItemPosition()==0)parametri.put(KEY_ID_YEAR_TO, "0");
		else parametri.put(KEY_ID_YEAR_TO, YearsTo.get(spinnerYearTo.getSelectedItemPosition()));
		if(etPriceFrom.getText().toString().equals("")) parametri.put(KEY_ID_PRICE_FROM, "0");
		else parametri.put(KEY_ID_PRICE_FROM, etPriceFrom.getText().toString());
		if(etPriceTo.getText().toString().equals("")) parametri.put(KEY_ID_PRICE_TO, "0");
		else parametri.put(KEY_ID_PRICE_TO, etPriceTo.getText().toString());
		
		Intent intent = new Intent(MainActivity.this, AdListActivity.class);
		intent.putExtra(KEY_PARAM, parametri);
		startActivity(intent);
	}
	
	public static String getSoapURL(Context c) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(c);
		String SOAP_URL = sharedPref.getString(SettingsActivity.KEY_SOAP_URL, "212.178.249.219");
		return SOAP_URL;
		

	}
	
}



