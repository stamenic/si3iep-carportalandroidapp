package rs.etf.iep.autooglasi;

import java.text.SimpleDateFormat;
import java.util.HashMap;

import rs.etf.iep.autooglasi.helper.ImageLoader;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

public class AdActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ad_activity);
		Intent intent = getIntent();    
		Ad a = (Ad) intent.getSerializableExtra(MainActivity.KEY_ID_AD);
		populatePage(a);	
		}

	private void populatePage(Ad a) {
		TextView adMaker= (TextView) findViewById(R.id.AdMaker);
		TextView adModel= (TextView) findViewById(R.id.AdModel);
		TextView adYear = (TextView) findViewById(R.id.AdYear);
		TextView adBody = (TextView) findViewById(R.id.AdBody);
		TextView adFuel = (TextView) findViewById(R.id.AdFuel);
		TextView adPrice = (TextView) findViewById(R.id.AdPrice);
		TextView adDate = (TextView) findViewById(R.id.AdDate);
		ImageView adImage = (ImageView) findViewById(R.id.AdImage);
		ImageLoader img = new ImageLoader(this);
		
		adMaker.setText(a.getMaker());
		adModel.setText(a.getModel());
		adYear.setText(a.getYear()+"");
		adBody.setText(a.getBody());
		adFuel.setText(a.getFuel());
		adPrice.setText(a.getPriceString()+ "�");
		adDate.setText(a.getAdDateString());
		img.displayImage("http://109.106.254.31:8080/images/ads/"+a.getIDPicture()+".jpg", adImage);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ad, menu);
		return true;
	}

}
