package rs.etf.iep.autooglasi;

import android.annotation.SuppressLint;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Ad implements Serializable {

	
	public Ad() {
		// TODO Auto-generated constructor stub
	}

	private int IDAd;
	private String Maker;
	private String Model;
	private String Body;
	private String Fuel;
	private float Price;
	private int Year; 
	private String IDPicture;
	private Date AdDate;
	
		
	public Ad(int iDAd, String maker, String model, String body, String fuel,
			float price, int year, String iDPicture, Date adDate) {
		super();
		IDAd = iDAd;
		Maker = maker;
		Model = model;
		Body = body;
		Fuel = fuel;
		Price = price;
		Year = year;
		IDPicture = iDPicture;
		AdDate = adDate;
	}
	
	
	public int getIDAd() {
		return IDAd;
	}
	public void setIDAd(int iDAd) {
		IDAd = iDAd;
	}
	public String getMaker() {
		return Maker;
	}
	public void setMaker(String maker) {
		Maker = maker;
	}
	public String getModel() {
		return Model;
	}
	public void setModel(String model) {
		Model = model;
	}
	public String getBody() {
		return Body;
	}
	public void setBody(String body) {
		Body = body;
	}
	public String getFuel() {
		return Fuel;
	}
	public void setFuel(String fuel) {
		Fuel = fuel;
	}
	public float getPrice() {
		return Price;
	}
	
	@SuppressLint("DefaultLocale")
	public String getPriceString() {
		 return String.format("%d",(int)Price);
	}
	
	public void setPrice(float price) {
		Price = price;
	}
	public int getYear() {
		return Year;
	}
	public void setYear(int year) {
		Year = year;
	}
	public String getIDPicture() {
		return IDPicture;
	}
	public void setIDPicture(String iDPicture) {
		IDPicture = iDPicture;
	}
	public Date getAdDate() {
		return AdDate;
	}
	public void setAdDate(Date adDate) {
		AdDate = adDate;
	}
	public String getAdDateString() {
		SimpleDateFormat dateFormat =  new SimpleDateFormat("dd.MM.yyyy");
		return dateFormat.format(AdDate);
	}
	
	
	
}
